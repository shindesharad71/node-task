# Node Rest APIs - Task (CRUD, JWT Auth & Balanced Algorithm Test)

This is an assignment for NodeJS developer interview.

### Installation and Setup

1. Clone this repository.
2. `cd` into folder.
3. Run `npm i` to install all dependency.
4. Rename `.env.ref` to `.env` and update your **MongoDB credentials**.
5. Run `npm run dev` to start node server.

### API Docs

**There is a `Node-Task.postman_collection.json` file in repository which can be directly imported into postman for faster testing.**

**API Base URL -** http://localhost:3000/

|  Endpoint | Method  |  Body | Result  | Auth Required?  |
|---|---|---|---|---|
| users/register  | POST  |  email, username, password, dateOfBirth, role  | Creates user  | No  |
|  users/login |  POST | email, password  |  User Login |  No |
|  balanced | POST  | input  | Check for balanced pattern  | Yes  |
| admin | GET | - | Get list of all users | Yes |
| admin | DELETE | username | Delete user | Yes |


_Thanks & Regards_  
Sharad Shinde
https://sharadshinde.in