import mongoose from 'mongoose';

const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

type UserType = mongoose.Document & {
    id: string,
    email: string,
    username: string,
    password: string,
    dateOfBirth: string,
    role: string,
    attempts: number
};

const userSchema = new Schema({
    id: { type: ObjectId, index: true },
    email: {
        type: String, unique: true, required: 'required email', index: true, lowercase: true
    },
    password: { type: String, required: 'required password' },
    username: {
        type: String, unique: true, required: 'required username', index: true, lowercase: true
    },
    dateOfBirth: { type: Date, required: 'required dateOfBirth' },
    role: { type: String, required: 'required role' },
    attempts: { type: Number, default: 0 }
}, {
        timestamps: true
    });

const User = mongoose.model('User', userSchema);

export { UserType, User };
