import { User, UserType } from '../user/userModel';
import { Balanced, BalancedType } from './balancedModel';

const balancedProcess = async (req: any, res: any) => {
    try {
        let userResponse;
        const isPatternCorrect = checkBalancedPattern(req.body.input);
        const updateAttemptQuery = { username: req.user.username };
        const updateAttemptDoc = { $inc: { attempts: 1 } };
        const { attempts } = await User.findOneAndUpdate(updateAttemptQuery, updateAttemptDoc, { new: true }) as UserType;

        if (isPatternCorrect) {
            const balanced = new Balanced({
                username: req.user.username,
                message: 'Success'
            });
            await balanced.save();
            userResponse = {
                username: req.user.username,
                message: 'balanced',
                attempts
            };
        } else {
            userResponse = {
                username: req.user.username,
                message: 'unbalanced',
                attempts
            };
        }
        res.send(userResponse);
    } catch (error) {
        res.status(400).json({ error: error.name, message: error.message });
        throw error;
    }
};

function checkBalancedPattern(pattern: string) {
    try {
        let matchingOpeningBracket;
        const stack = [];
        const openingBrackets = ['[', '{', '('];
        const closingBrackets = [']', '}', ')'];

        if (pattern.length <= 1) {
            return false;
        }

        for (const i of pattern) {
            if (closingBrackets.indexOf(i) > -1) {
                matchingOpeningBracket = openingBrackets[closingBrackets.indexOf(i)];
                if (stack.length === 0 || (stack.pop() !== matchingOpeningBracket)) {
                    return false;
                }
            } else {
                stack.push(i);
            }
        }
        return (stack.length === 0);
    } catch (error) {
        throw error;
    }
}

export { balancedProcess };
