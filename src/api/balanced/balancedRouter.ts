import express from 'express';
import { balancedProcess } from './balancedController';

const feedRouter = express.Router();

feedRouter.post('/', balancedProcess);

export default feedRouter;
