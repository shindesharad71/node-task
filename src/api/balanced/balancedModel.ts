import mongoose from 'mongoose';

const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

type BalancedType = mongoose.Document & {
    id: string,
    username: string,
    message: string
};

const balancedSchema = new Schema({
    id: { type: ObjectId, index: true },
    username: {
        type: String
    },
    message: { type: String }
}, {
        timestamps: true
    });

const Balanced = mongoose.model('Balanced', balancedSchema);

export { BalancedType, Balanced };
