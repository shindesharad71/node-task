import express from 'express';
import { getUsers, removeUser } from './adminController';

const adminRouter = express.Router();

adminRouter.get('/', getUsers);
adminRouter.delete('/', removeUser);

export default adminRouter;
