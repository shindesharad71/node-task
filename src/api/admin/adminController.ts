import { User, UserType } from '../user/userModel';

const getUsers = async (req: any, res: any) => {
    try {
        if (req.user && req.user.role === 'admin') {
            const users = await User.find({}, '-password -attempts');
            res.send(users);
        } else {
            res.status(403).json({ message: `invalid role or access token` });
        }
    } catch (error) {
        res.status(400).json({ error: error.name, message: error.message });
        throw error;
    }
};

const removeUser = async (req: any, res: any) => {
    try {
        if (req.user && req.user.role === 'admin') {
            if (req.user.username === req.body.username) {
                res.status(403).json({ message: `sorry! you can't delete yourself!` });
            } else {
                await User.deleteOne({ username: req.body.username });
                res.send({ message: 'user removed user successfully' });
            }
        } else {
            res.status(403).json({ message: `invalid role or access token` });
        }
    } catch (error) {
        res.status(400).json(error);
        throw error;
    }
};

export { getUsers, removeUser };
