import adminRouter from './admin/adminRouter';
import balancedRouter from './balanced/balancedRouter';
import userRouter from './user/userRouter';

const Routes = [
    {
        path: '/users',
        router: userRouter
    },
    {
        path: '/balanced',
        router: balancedRouter
    },
    {
        path: '/admin',
        router: adminRouter
    }
];

export { Routes };
